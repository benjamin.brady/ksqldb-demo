KsqlDB Demo
=====

## What is KsqlDB
ksqlDB is an event streaming database purpose-built to help developers create stream processing applications on top of Apache Kafka® (an event streaming platform).

Based the dmeo off KsqlDB Quickstart docs: https://ksqldb.io/quickstart.html

Events are published from TradeMe Business2 EventTrackingService.cs

## Concepts
* __Streams__ are immutable, append-only collections. They're useful for representing a series of historical facts. Adding multiple events with the same key means that they are simply appended to the end of the stream.
* __Tables__ are mutable collections. They let you represent the latest version of each value per key. They're helpful for modeling change over time, and they're often used to represent aggregations. Materialized view.
* __Connectors__ ksqlDB is capable of using the vast ecosystem of Kafka Connect connectors through its SQL syntax. Huge range of connectors for virtually any store: SQLServer, Postgresql, Elastic, Redis, Salesforce, Snowplow, S3 Buckets etc.

## Diagram

![System Diagram](ksqldb-demo.svg)

## Run Kafka + KsqlDB
    docker-compose up

## Stream raw event data from Kafka to the console
    docker exec broker kafka-console-consumer --topic listing.create --from-beginning --bootstrap-server localhost:9092

## Run KsqlDB CLI
    docker exec -it ksqldb-cli ksql http://ksqldb-server:8088

## Exploring the database

    LIST topics;
    SHOW streams;
    LIST tables;
    DESCRIBE KSQL_PROCESSING_LOG; 

## Creating a stream composed from a topic
    CREATE STREAM listing (listingId BIGINT, listingIdStr VARCHAR, listingType VARCHAR, listingTitle VARCHAR) 
    WITH (kafka_topic='listing.create', value_format='json', partitions=1);
    
    CREATE STREAM bid (listingId BIGINT, listingIdStr VARCHAR, bidAmount DOUBLE) WITH (kafka_topic='bid',value_format='json');

    CREATE STREAM watchlist_add (listingId BIGINT, listingIdStr VARCHAR, memberId VARCHAR) WITH (kafka_topic='watchlist.add',value_format='json');


## Creating a table composed from streams
    CREATE TABLE listing_t
    AS
    SELECT 
        listingIdStr, 
        LATEST_BY_OFFSET(listingType) AS listingType, 
        LATEST_BY_OFFSET(listingTitle) AS listingTitle
    FROM listing
    GROUP BY listingIdStr EMIT CHANGES;

    CREATE TABLE listing_t (listingIdStr VARCHAR PRIMARY KEY, listingType VARCHAR, listingTitle VARCHAR)
    WITH (kafka_topic='listing.create', value_format='json');

    CREATE TABLE listing_enriched (listingId BIGINT PRIMARY KEY, listingType VARCHAR, listingTitle VARCHAR, bidAmount DOUBLE, watchers INTEGER);

## Query Examples

    ksql> select * from listing emit changes;
    +----------------------------------+----------------------------------+----------------------------------+----------------------------------+
    |LISTINGID                         |LISTINGIDSTR                      |LISTINGTYPE                       |LISTINGTITLE                      |
    +----------------------------------+----------------------------------+----------------------------------+----------------------------------+
    |2147581324                        |2147581324                        |AUCTION                           |1968 Land Rover Series 3          |
    |2147581325                        |2147581325                        |AUCTION                           |easy pet rabbit guinea pig cages  |

### Join two streams within time window

    SELECT l.listingId, l.listingTitle, LATEST_BY_OFFSET(bid.bidAmount) as latest_bid, count(*) AS bid_count
    FROM listing  l
    JOIN bid WITHIN 5 MINUTES ON bid.listingId = l.listingId
    GROUP BY l.listingId, l.listingTitle
    EMIT CHANGES;

### Join bid stream to listing table 

    SELECT l.listingIdStr, l.listingTitle, LATEST_BY_OFFSET(bid.bidAmount) as latest_bid, count(*) AS bid_count
    FROM bid  
    JOIN listing_t l ON bid.listingIdStr = l.listingIdStr
    GROUP BY l.listingIdStr, l.listingTitle
    EMIT CHANGES;

### Join watchlist stream to listing table

    SELECT l.listingIdStr, l.listingTitle, count(*) AS watch_count
    FROM watchlist_add  w
    JOIN listing_t l ON w.listingIdStr = l.listingIdStr
    GROUP BY l.listingIdStr, l.listingTitle

## Connector Example

### ElasticSearch
Create a sink to elastic search.

    CREATE SINK CONNECTOR elastic_listing_writer WITH (
        'connector.class' = 'io.confluent.connect.elasticsearch.ElasticsearchSinkConnector',
        'connection.url' = 'http://elasticsearch:9200',
        'type.name' = 'kafka-connect',
        'topics' = 'listing.create',
        'schema.ignore' = 'true',
        'key.ignore'='true'
    );
Verify it doesn't have errors by running `DESCRIBE CONNECTOR elastic_listing_writer;`
The data should now show up in Kibana (http://localhost:5601/).

